mate-menu (22.04.2-2) unstable; urgency=medium

  * debian/patches:
    + Add 0001_fix-upstream-version.patch, 0002_fix-categories-without-
      icon.patch (cherry-picked from upstream PRs).
    + Add 1001_py-setup-clean-i18n.patch: Stop using clean_i18n class from
      distutils.extra. (Closes: #1054802).
  * debian/control:
    + Bump Standards-Version: to 4.6.2. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 05 Nov 2023 14:02:54 +0000

mate-menu (22.04.2-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 03 Apr 2022 21:31:06 +0200

mate-menu (22.04.1-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 25 Feb 2022 22:38:03 +0100

mate-menu (22.04.0-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.

  [ Mike Gabriel ]
  * debian/control:
    + Bump Standards-Version: to 4.6.0. No changes needed.
    + Bump DH compat level to version 13.
  * debian/watch:
    + Use format version 4.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 27 Jan 2022 12:14:00 +0100

mate-menu (20.04.3-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 16 Apr 2020 09:57:30 +0200

mate-menu (20.04.1-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 17 Feb 2020 22:31:10 +0100

mate-menu (20.04.0-1) unstable; urgency=medium

  [ Mike Gabriel ]
  * debian/control:
    + Bump Standards-Version: to 4.5.0. No changes needed.
  * debian/upstream/metadata:
    + Drop obsolete fields Contact: and Name:.
    + Append .git suffix to URLs in Repository: field.

  [ Martin Wimpress ]
  * New upstream release.
  * debian/patches:
    + Drop 1001_check-availability-of-env-vars.patch. Applied upstream.
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 14 Feb 2020 21:07:50 +0100

mate-menu (19.10.2-2) unstable; urgency=medium

  * debian/control:
    + Bump Standards-Version: to 4.4.1. No changes needed.
    + Add Rules-Requires-Root: field and set it to no.
    + Add D (mate-menu): python3-gi-cairo. (Closes: #942676).
  * debian/patches:
    + Add 1001_check-availability-of-env-vars.patch. Fix
      occurring exception (crash) when a user clicks the
      "Edit properties" item in context menu of applications
      list. (LP:#1845816). Thanks to Nicholas Guriev for
      providing the patch.
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 31 Oct 2019 21:33:36 +0100

mate-menu (19.10.2-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release. (LP: #1845786)

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 28 Sep 2019 23:58:19 +0200

mate-menu (19.10.1-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
  * debian/patches:
    + Drop 1001_drop-duplicate-plugin-py-files.patch. Applied upstream.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 20 Aug 2019 08:36:28 +0200

mate-menu (19.10.0-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
  * debian/control:
    + Update to Python3 and gir.
    + Replace D: gir1.2-mate-panel-2.0 (>= 1.20) with
      gir1.2-matepanelapplet-4.0 (>= 1.22). (Closes: #926447).
  * debian/rules:
    + Migrate to Python3.
  * debian/patches:
    + Drop 1001_convert-gvfs-open-to-gio-open.patch. Applied upstream.

  [ Mike Gabriel ]
  * debian/{compat,control}:
    + Use debhelper-compat notation. Bump to DH compat level version 12.
  * debian/control:
    + Bump Standards-Version: to 4.4.0. No changes needed.
  * debian/upstream/metadata:
    + Add file. Comply with DEP-12.
  * debian/copyright:
    + Update copyright attributions.
  * debian/patches:
    + Add 1001_drop-duplicate-plugin-py-files.patch. Drop duplicate Python
      files for the menu plugin system. (Closes: #931004).

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 02 Aug 2019 17:41:12 +0200

mate-menu (18.04.3-3) unstable; urgency=medium

  * debian/control:
    + Update Vcs-*: fields. Package has been migrated to salsa.debian.org.
    + Bump Standards-Version: to 4.1.4. No changes needed.
    + Drop pkg-mate-team Alioth mailing list from Uploaders: field.
  * debian/copyright:
    + Make Upstream-Name: field's value more human readable.
    + Update mail address in Upstream-Contact: field.
    + Update URL in Source: field.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 09 Jul 2018 07:57:55 +0200

mate-menu (18.04.3-2) unstable; urgency=medium

  * debian/patches:
    + Add 1001_convert-gvfs-open-to-gio-open.patch. Convert deprecated
      gvfs-open call to "gio open".
  * debian/control:
    + Drop from D (mate-menu): libgvfs-bin. (Closes: #877743).

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 18 Apr 2018 13:12:57 +0200

mate-menu (18.04.3-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
  * debian/control:
    + Update D: gir1.2-matedesktop-2.0 (mate-menu).

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 18 Apr 2018 12:30:15 +0200

mate-menu (18.04.2-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 18 Mar 2018 17:56:04 +0100

mate-menu (18.04.1-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.

  [ Mike Gabriel ]
  * debian/{control,compat}:
    + Bump DH version level to 11.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 11 Mar 2018 12:09:26 +0100

mate-menu (18.04.0-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
  * debian/copyright:
    + Update copyright attributions.

  [ Mike Gabriel ]
  * debian/copyright:
    + Use secure URI for copyright format.

  [ Vangelis Mouhtsis ]
  * debian/control:
    + Temporarily have pkg-mate-team ML under Uploaders:.
    + Update Maintainer: field to debian-mate ML on lists.debian.org.
    + Rename pretty name of our team -> Debian+Ubuntu MATE Packaging Team.
    + Bump Standards-Version: to 4.1.3. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 15 Feb 2018 10:57:33 +0100

mate-menu (17.10.8-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 25 Sep 2017 10:57:04 +0200

mate-menu (17.10.7-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * debian/control: Update D (mate-menu): mozo | menulibre.
  * debian/control: Update build deps for MATE 1.18.
  * debian/control: Add D python-setproctitle.
  * debian/control: Point Homepage at Github.
  * debian/watch: Get releases from Github.
  * debian/copyright: Update copyright attributions.

  [ Aron Xu ]
  * New upstream release.

  [ Vangelis Mouhtsis ]
  * debian/compat:
    + Update compat version.
  * debian/control:
    + Bump debhelper version to (>= 10.3~).
  * debian/control:
    + Bump Standards-Version: to 4.0.0. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 25 Jul 2017 11:27:57 +0200

mate-menu (16.10.1-2) unstable; urgency=medium

  * debian/watch:
    + Adapt to Bitbucket's new download URL scheme.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 07 Oct 2016 10:58:31 +0200

mate-menu (16.10.1-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.

  [ Vangelis Mouhtsis ]
  * debian/copyright: Update copyright: Add missing files.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 12 Jun 2016 21:55:56 +0200

mate-menu (16.10.0-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release. (Closes: #826349).
  * debian/control:
    + Bump Standards: to 3.9.8. No changes needed.
    + Add to D (mate-menu): gir1.2-gtk-3.0.
    + Versioned D (mate-menu): gir1.2-mate-panel (>= 1.14).
    + Versioned D (mate-menu): mate-menus (>= 1.14).
    + Versioned D (mate-menu): mozo (>= 1.14).
    + Remove D (mate-menu): python-gtk2. MATE Menu has been ported to GTK3+.
    + Remove D (mate-menu): python-glade2.
  * debian/copyright:
    + Update copyright attributions.
    + Remove capi.py from copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 06 Jun 2016 16:24:50 +0200

mate-menu (5.7.1-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
    - Fix configuring a custom heading colour. (LP:#1560332).
    - Display the Software Boutique as package manager if it is
      available. (LP:#1568170).
    - Ensure the menu is always drawn over existing windows.
      (LP:#1569563)

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 14 Apr 2016 13:38:02 +0200

mate-menu (5.7.0-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
    - Fixes the menu being offset from the panel. (LP:#1559371).

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 08 Apr 2016 10:21:20 +0200

mate-menu (5.6.9-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
    - Fixes mate-menu.py being displayed. Closes (LP: #1553313)
    - Updated translations.

  [ Mike Gabriel ]
  * debian/control:
    + Bump Standards: to 3.9.7. No changes needed.
    + Use encrypted URLs for Vcs-*: field.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 12 Mar 2016 15:08:39 +0100

mate-menu (5.6.8-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
    - Fix breakage due to wrong LSB release file ending in previous
      upstream release. (Closes: #813130).

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 03 Feb 2016 19:27:37 +0100

mate-menu (5.6.7-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 22 Jan 2016 10:39:30 +0100

mate-menu (5.6.6-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release. (LP:#1462483).

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 31 Dec 2015 14:38:33 +0100

mate-menu (5.6.5a-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
    + Updated translations.
  * debian/copyright:
    + Update copyright attribution for translators.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 11 May 2015 13:38:27 +0200

mate-menu (5.6.4-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
    - Updated translations.
    - Fixed UnboundLocalError in refreshTrash(). Closes (LP: #1428273)
  * debian/control
    + Updated Homepage
  * debian/watch
    + Updated watch URL.

  [ Mike Gabriel ]
  * debian/copyright:
    + Update URL in Upstream-Source: field.
  * debian/control:
    + Add to B-D: dh-python.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 25 Mar 2015 15:38:34 +0100

mate-menu (5.6.3-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
    + Added translations.
    + Removed package management features.
    + Removed useless imports and dead code.
    + Refactored some os.system() calls to Pythonic equivalents.
  * debian/copyright:
    + Update copyright attribution for translators.
  * debian/patches:
    + Removed 1001_no-version-py-anymore.patch, now fixed upstream.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 04 Mar 2015 09:51:42 +0100

mate-menu (5.6.2-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
  * debian/copyright:
    + Remove COPYING from copyright.
    + Update copyright attribution for new mate-logo.svg.
    + Remove obsolete entries from copyright.

  [ Mike Gabriel ]
  * debian/control:
    + Add to D (mate-menu): libglib2.0-bin (for glib-compile-schema in postinst
      script). (Closes: #779102).
  * debian/patches:
    + Add 1001_no-version-py-anymore.patch. Fix broken po/POTFILES.in after
      version.py has got removed by upstream.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 27 Feb 2015 19:38:23 +0100

mate-menu (5.6.1-2) unstable; urgency=medium

  * Packaging scripts:
    + Drop duplicate postinst script.
  * debian/copyright:
    + Use more appropriate license short name "Expat"
      instead of "MIT".

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 19 Feb 2015 16:34:11 +0100

mate-menu (5.6.1-1) unstable; urgency=low

  * Initial release (Closes: #777056).

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 19 Feb 2015 14:59:57 +0100
